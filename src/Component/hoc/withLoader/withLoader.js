import React, {useState, useEffect} from 'react';
import Spinner from "../../Spinner/Spinner";

const withLoader = (Component, axios) => {
    return props => {
        const [loading, setLoading] = useState(false);

        useEffect(()=> {
            axios.interceptors.request.use(req => {
                console.log('req')
                setLoading(true);
                return req;
            });

            axios.interceptors.response.use(res => {
                console.log('res')
                setLoading(false);
                return res;
            });
        }, []);

        return (
            <>
                {loading ? <Spinner /> : <Component {...props} />}
            </>
        );
    };
};

export default withLoader;