import React, {useState} from "react";
import axios from "axios";
import withLoader from "../../Component/hoc/withLoader/withLoader";
import List from "../../Component/List/List";
import './App.css';

const App = () => {

    const [data, setData] = useState([]);

    const fetchData = async () => {
        const response = await axios.get('https://classwork-63-burger-default-rtdb.firebaseio.com/quotes.json');
        console.log(response);
        const value = Object.values(response.data);
        setData([...value]);
    };

    let quotes = <p>nope</p>;

    if (data) {
        quotes = data.map(q => (
            <List key={q.author}>{q.text}</List>
        ));
    }

    return (
        <div className="App">
            <button onClick={fetchData} className="btn">Click</button>
            {quotes}
        </div>
    );
};

export default withLoader(App, axios);
